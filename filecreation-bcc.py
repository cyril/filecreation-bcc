#! /usr/bin/python2
# -*- encoding: utf-8 -*-
# @lint-avoid-python-3-compatibility-imports
#
# file-creation    Trace the creation of files in the VFS
#                  For Linux, uses BCC, eBPF. Embedded C.
#
# This program tracks the creation of files in the VFS,
# and determines (hopefully!) there full path and information
# linked to the event creation.
#
# This program was first based on filelife-bcc, hence the copyright
#
# USAGE: filecreation [-h] [-p PID]
#
# Copyright 2016 Netflix, Inc.
# Licensed under the Apache License, Version 2.0 (the "License")
#
# 08-Feb-2015   Brendan Gregg   Created this.
# 17-Feb-2016   Allan McAleavy updated for BPF_PERF_OUTPUT
# 22-Aug-2019   Cyril Duval modified to track file creation

from __future__ import print_function
from bcc import BPF
import argparse
from time import strftime
import ctypes as ct
from datetime import datetime
import requests
import email.Header

WEBHOOK_URL = "https://hooks.slack.com/services/<changeme>"

# arguments
examples = """examples:
    ./filelife           # trace all stat() syscalls
    ./filelife -p 181    # only trace PID 181
"""
parser = argparse.ArgumentParser(
    description="Trace stat() syscalls",
    formatter_class=argparse.RawDescriptionHelpFormatter,
    epilog=examples)
parser.add_argument("-p", "--pid",
    help="trace this PID only")
parser.add_argument("-n", "--name",
    help="trace this program name only")
parser.add_argument("--ebpf", action="store_true",
    help=argparse.SUPPRESS)
args = parser.parse_args()
debug = 0

# define BPF program
bpf_text = """
#include <uapi/linux/ptrace.h>
#include <linux/fs.h>
#include <linux/sched.h>

struct data_t {
    u64 ts;
    u32 pid;
    char comm[TASK_COMM_LEN];
    char fname[DNAME_INLINE_LEN];
};

BPF_PERF_OUTPUT(events);

int trace_create(struct pt_regs *ctx, struct inode *dir, struct dentry *dentry)
{
    struct data_t data = {};
    u32 pid = bpf_get_current_pid_tgid();
    FILTER

    u64 ts = bpf_ktime_get_ns();
   
    struct qstr d_name = dentry->d_name;
    if (d_name.len == 0)
        return 0;

    if (bpf_get_current_comm(&data.comm, sizeof(data.comm)) == 0) {
        data.pid = pid;
    }

    struct dentry *parent = dentry;
    
    if (parent == NULL)
      return 0;
    
    // This thing actually sucks. Unfortunately, as in 2019 there is
    // apparently no other way of getting the full PATH of a struct dentry,
    // and loops aren't allowed in eBPF, even if there are bounded.

    // I hope a better will be found soon. This implemtation covers *most*
    // of the files on the VFS since most of them are in less than 14 layers
    // of subdirectory, but still *sucks*
    PATH
    PARENT
    PATH
    PARENT
    PATH
    PARENT
    PATH
    PARENT
    PATH
    PARENT
    PATH
    PARENT
    PATH
    PARENT
    PATH
    PARENT
    PATH
    PARENT
    PATH
    PARENT
    PATH
    PARENT
    PATH
    PARENT
    PATH
    PARENT
    PATH
    return 0;
};
"""

TASK_COMM_LEN = 16            # linux/sched.h
DNAME_INLINE_LEN = 255        # linux/dcache.h

class Data(ct.Structure):
    _fields_ = [
        ("ts", ct.c_ulonglong),
        ("pid", ct.c_uint),
        ("comm", ct.c_char * TASK_COMM_LEN),
        ("fname", ct.c_char * DNAME_INLINE_LEN)
    ]

if args.pid:
    bpf_text = bpf_text.replace('FILTER',
        'if (pid != %s) { return 0; }' % args.pid)
else:
    bpf_text = bpf_text.replace('FILTER', '')
bpf_text = bpf_text.replace("PATH", 'd_name = parent->d_name; bpf_probe_read(data.fname, sizeof(data.fname), d_name.name); data.ts = ts; events.perf_submit(ctx, &data, sizeof(data));')
bpf_text = bpf_text.replace('PARENT', 'parent = parent->d_parent; if (parent == NULL || parent == parent->d_parent) {return 0;}')
if debug or args.ebpf:
    print(bpf_text)
    if args.ebpf:
        exit()

# initialize BPF
b = BPF(text=bpf_text)
b.attach_kprobe(event="vfs_create", fn_name="trace_create")
# newer kernels (say, 4.8) may don't fire vfs_create, so record (or overwrite)
# the timestamp in security_inode_create():
b.attach_kprobe(event="security_inode_create", fn_name="trace_create")

paths = {}

class news():
    def __init__(self, path):
        self.path = path
        try:
          with open(path) as f:
            news_raw = f.read()
            news = news_raw.split("\n")
            self.error = False
        except Exception as e:
            self.error = True
            return
        self.headers = {}

        for i in range(len(news)):
          line = news[i]
          if line == '':
            break # end of headers
          if line[0] == ' ' or line[0] == '\t':
              continue
          header = line.split(": ", 1)
          try: # Multiline header
              if news[i+1][0] == ' ':
                  header[1] += ' ' + news[i+1]
          except:
              pass
          try:
            decoded_el = email.Header.decode_header(header[1])
            self.headers[header[0].lower()] = "".join([el[0].decode('utf-8', errors='ignore') for el in decoded_el])
          except Exception as e:
            print("\t> " + str(e))
            pass
        self.body = news_raw.split("\n\n", 1)[1].decode("utf-8", errors="ignore")

    def __repr__(self):
        return "News posted in *" + self.headers.get("newsgroups", "<unknown>") + "*, _" + self.headers.get("date", "I don't know when :(") + "_\n" + \
               "Posted by *" + self.headers.get("from", "<unknown>") + "*\n" + \
               "`" + self.headers.get("subject", "<no subject>") + "`\n" + \
               "```" + self.body + "```"

    def send(self):
        global WEBHOOK_URL
        if self.error:
          return
        try:
            print("sending")
            data = {"payload":'{"text":"'+self.__repr__().replace('"','\\"')+u'","username":"Méléagant, le crieur public","icon_url":"https://static.cri.epita.fr/icon.png"}'}
            r = requests.post(WEBHOOK_URL, data=data)
            print(data)
            print(r.text)
            if r.status_code != 200:
                requests.post(WEBHOOK_URL, data={"payload":'{"text":"*news posted*\n_No more information available at that time ..._"}'})
        except requests.exceptions.RequestException as e:
            print(e)
            requests.post(WEBHOOK_URL, data={"payload":'{"text":"*news posted*\n_No more information available at that time ..._"}'})



def extract(cpu, data, size):
    event = ct.cast(data, ct.POINTER(Data)).contents
    if args.name and not args.name in event.comm.decode('utf-8', "replace"):
      return
    if not event.ts in paths:
      paths[event.ts] = {"pid":event.pid, "paths":[], "name":event.comm.decode('utf-8', "replace"), "time": datetime.now()}
    paths[event.ts]["paths"].append(event.fname.decode('utf-8', "replace"))

def handle_news(path):
    event = news(path)
    event.send()

def print_extract():
    for k, event in paths.items():
        if (datetime.now() - event["time"]).total_seconds() > 0.01:
            event["paths"].reverse()
            print(event["name"]+'('+str(event["pid"])+') ', end='')
            def_path = ""
            for path in event["paths"]:
              def_path += "/"+path
              print("/"+path, end='')
            print()
            if "/var/spool/news/articles" in def_path:
                handle_news(def_path)
            del paths[k]

b["events"].open_perf_buffer(extract)
while 1:
    try:
        b.perf_buffer_poll(1000)
        print_extract()
    except KeyboardInterrupt:
        exit()
